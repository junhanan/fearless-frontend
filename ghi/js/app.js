// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();

//             const conferenceDetail = details.conference;
//             const descriptionTag = document.querySelector('.card-text');
//             descriptionTag.innerHTML = conferenceDetail.description;

//             const pictureUrl = details.conference.location.picture_url;
//             const imageTag = document.querySelector('.card-img-top');
//             imageTag.src = pictureUrl
//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });

function formatDate(dateString) {
    const date = new Date(dateString);
    const options = { year: 'numeric', month: 'long', day: 'numeric', timeZone: "UTC" };
    return date.toLocaleDateString('en-US', options);
  }

function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    const formattedStartDate = formatDate(startDate);
    const formattedEndDate = formatDate(endDate);

    return `
<div class = "col-4">
    <div class="card shadow p-3 mb-5 bg-body-tertiary rounded h-100">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class= "text-muted">${locationName}</p>
                <p class="card-text">${description}</p>
            </div>
                <div class= "card-footer">${formattedStartDate} - ${formattedEndDate}</div>
            </div>
    </div>
</div>
    `;
}









window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error('Failed to grab conference boo hooo')
        } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;

                const startDate = details.conference.starts;
                const endDate = details.conference.ends;
                const locationName = details.conference.location.name;

                const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);

                const column = document.querySelector('.col');
                column.innerHTML += html;
            }
        }

    }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
        const errorContainer = document.querySelector('.error-container');
        errorContainer.innerHTML = `
        <div class="alert alert-danger" role="alert">
            Faillll an error corrused. Try again
        </div>
        `
    }

});
