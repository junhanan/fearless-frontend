window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        loadingIcon.classList.add('d-none'); // Add the 'd-none' class to hide the loading icon
        selectTag.classList.remove('d-none');
    }

});

const formTag = document.getElementById("create-attendee-form");
const successMessage = document.getElementById("success-message");

formTag.addEventListener("submit", async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData))

    const attendeeUrl = "http://localhost:8001/api/attendees/";

    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            "Content-Type": "application/json"
        }
    }
    const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
    console.log("dsiojdsoijdsoij", attendeeResponse)
    if (attendeeResponse.ok) {
        formTag.reset();
        const newAttendee = await attendeeResponse.json();
        successMessage.classList.remove('d-none');
    }
})


// http://localhost:8001/api/conferences/2/attendees/
