import React from 'react';
import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <>
    <div className="container">

      <nav className="table table-striped">
        <ul className="nav">
          <li className="nav-item">
            <a className="nav-link" href="/">Home</a>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/locations/new">New location</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/attendees/new">New attend conference</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/conferences/new">New conference</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/attendees">Attendee List</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/presentations/new">New Presentation</NavLink>
          </li>
        </ul>
      </nav>
      </div>
    </>
  );
}

export default Nav;
