import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');

const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
    }
};

useEffect(() => {
    fetchData();
}, []);

const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const jsonData = JSON.stringify(Object.fromEntries(formData));
    console.log(jsonData);

    const presentationUrl = 'http://localhost:8000/api/conferences/1/presentations/';
    const fetchConfig = {
        method: 'post',
        body: jsonData,
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const presentationResponse = await fetch(presentationUrl, fetchConfig);

    if (presentationResponse.ok) {
        event.target.reset();
        const newPresentation = await presentationResponse.json();
        console.log(newPresentation);
    }
};

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input
                    placeholder="Presenter name"
                    required
                    type="text"
                    name="presenter_name"
                    id="presenter_name"
                    className="form-control"
                    value={presenterName}
                    onChange={(e) => setPresenterName(e.target.value)}
                    />
                    <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    placeholder="Presenter email"
                    required
                    type="email"
                    name="presenter_email"
                    id="presenter_email"
                    className="form-control"
                    value={presenterEmail}
                    onChange={(e) => setPresenterEmail(e.target.value)}
                    />
                    <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    placeholder="Company name"
                    type="text"
                    name="company_name"
                    id="company_name"
                    className="form-control"
                    value={companyName}
                    onChange={(e) => setCompanyName(e.target.value)}
                    />
                    <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    placeholder="Title"
                    required
                    type="text"
                    name="title"
                    id="title"
                    className="form-control"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    />
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea
                    className="form-control"
                    id="synopsis"
                    rows="3"
                    name="synopsis"
                    value={synopsis}
                    onChange={(e) => setSynopsis(e.target.value)}
                    ></textarea>
                </div>
                <div className="mb-3">
                    <select
                    required
                    name="conference"
                    id="conference"
                    className="form-select"
                    >
                    <option value="">Choose a conference</option>
                    {conferences.map((conference) => (
                        <option
                        key={conference.href}
                        value={conference.href}
                        >
                        {conference.name}
                        </option>
                    ))}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    );
}

export default PresentationForm;
